---
author: Paweł Talar
title: Rammstein
subtitle: eine deutsche Rockband
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---

## Neue Deutsche Härte - eine Musikstil
Die Texte sind überwiegend in deutscher Sprache abgefasst, die musikalischen Einflüsse sind vielfältig. Die Standardbesetzung bilden (tiefer) Gesang, E-Gitarre, E-Bass, Schlagzeug und Keyboard. 
![Band logos](images/bands.jpg)

## Aktuelle Besetzung
* Till Lindemann - Gesang, Mundharmonika
* Richard Z. Kruspe - E-Gitarre, Begleitgesang
* Paul Landers - Gitarre, Hintergrundgesang
* Oliver Riedel - Bass, Hintergrundgesang
* Christoph Schneider - Perkussion, Schlagzeug
* Christian “Flake” Lorenz - Keyboard, Klavier, Synthesizer
![Rammstein members](images/rammstein.jpg)

## Diskographie    
| Titel                | Jahre  |
| ---------------------|:-----:|
| Herzeleid            | 1995  |
| Sehnsucht            | 1997  |
| Mutter               | 2001  | 
| Reise, Reise         | 2004  |
| Rosenrot             | 2005  |
| Liebe ist für alle da| 2009  |
| Rammstein            | 2019  |